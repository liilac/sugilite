package main

import (
	"time"
)

// ScaledRandomisedTicker is a ticker that ticks at a random interval within a given range
type ScaledRandomisedTicker struct {
	// embed the general randomised ticker
	RandomisedTicker
	// list of scaling multipliers to use
	// Multipliers[tickNumber] = multiplier
	Multipliers TickMultipliers
}

// returns a new initialised random ticker instance
func NewScaledRandomisedTicker(intervals IntervalRange, multipliers TickMultipliers) *ScaledRandomisedTicker {
	// create a new ticker instance
	t := new(ScaledRandomisedTicker)
	// set the interval range
	t.Intervals.Maximum = intervals.Maximum
	t.Intervals.Minimum = intervals.Minimum
	// set the multipliers
	t.Multipliers = multipliers
	// initialise the ticker
	t.init()
	// return the ticker
	return t
}

// afterTick handles recording of statistics about the current tick and preparing of the next tick
func (t *ScaledRandomisedTicker) afterTick(ticked time.Time) {
	// record delay between intended and actual tick time
	delay := time.Now().Sub(t.nextTick)
	t.Delays = append(t.Delays, delay)
	// increment the tick counter
	t.Ticks++
	// ascertain the multiplier to be used for the next tick
	multiplier := t.Multipliers.Multiplier(int(t.Ticks))
	// add a random interval to the next tick time
	t.nextTick = t.nextTick.Add(t.Intervals.scaledRandomIntervalMandatory(multiplier))
}

// Start starts the ticker, with the first tick either being immediate, or after a random interval
func (t *ScaledRandomisedTicker) Start(now bool) {
	// set the next tick time to be now
	t.nextTick = time.Now()
	// if we were asked to add a random interval to the initial tick time
	if !now {
		multiplier := t.Multipliers.Multiplier(int(t.Ticks))
		t.nextTick = t.nextTick.Add(t.Intervals.scaledRandomIntervalMandatory(multiplier))
	}
	// start the tick loop
	go t.loop()
}
