package main

import (
	"time"
)

// RandomisedTicker is a ticker that ticks at a random interval within a given range
type RandomisedTicker struct {
	// number of ticks that have been sent so far
	Ticks uint64
	// channel for ticks
	C chan time.Time
	// channel for stop signal
	stop chan bool
	// channel for has stopped signal
	stopped chan time.Time
	// interval range to randomise within
	Intervals IntervalRange
	// array of differences between intended and actual tick time
	Delays []time.Duration
	// time to send the next tick
	nextTick time.Time
}

// returns a new initialised random ticker instance
func NewRandomisedTicker(intervals IntervalRange) *RandomisedTicker {
	// create a new ticker instance
	t := new(RandomisedTicker)
	// set the interval range
	t.Intervals.Maximum = intervals.Maximum
	t.Intervals.Minimum = intervals.Minimum
	// initialise the ticker
	t.init()
	// return the ticker
	return t
}

// init initialises the randomised ticker
func (t *RandomisedTicker) init() {
	// setup the tick channel
	t.C = make(chan time.Time)
	// setup the stop channel
	t.stop = make(chan bool)
	// setup the has stopped channel
	t.stopped = make(chan time.Time)
}

// afterTick handles recording of statistics about the current tick and preparing of the next tick
func (t *RandomisedTicker) afterTick(ticked time.Time) {
	// record delay between intended and actual tick time
	delay := time.Now().Sub(t.nextTick)
	t.Delays = append(t.Delays, delay)
	// increment the tick counter
	t.Ticks++
	// add a random interval to the next tick time
	t.nextTick = t.nextTick.Add(t.Intervals.randomIntervalMandatory())
}

// loop executes the actual ticking mechanism
func (t *RandomisedTicker) loop() {
	// loop indefinitely
	for {
		// wait until appropriate time to (potentially) tick
		time.Sleep(t.nextTick.Sub(time.Now()))
		// non blocking wait on channel receive
		select {
		// if able to send tick
		case t.C <- t.nextTick:
			t.afterTick(time.Now())
		// if stop signal received
		case <-t.stop:
			t.stopped <- time.Now()
			return
		}
	}
}

// Start starts the ticker, with the first tick either being immediate, or after a random interval
func (t *RandomisedTicker) Start(now bool) {
	// set the next tick time to be now
	t.nextTick = time.Now()
	// if we were asked to add a random interval to the initial tick time
	if !now {
		t.nextTick = t.nextTick.Add(t.Intervals.randomIntervalMandatory())
	}
	// start the tick loop
	go t.loop()
}

// Stop stops the ticker, blocking until the signal has been sent
// returns the time the loop actually stopped
func (t *RandomisedTicker) Stop() time.Time {
	// send the stop signal
	t.stop <- true
	// block until the loop has actually stopped
	return <-t.stopped
}

// Reset resets the statistics and counters for a ticker
func (t *RandomisedTicker) Reset() {
	// reset delays
	t.Delays = []time.Duration{}
	// reset counter
	t.Ticks = 0
}
