package main

import (
	"log"
	"math/rand"
	"time"

	"github.com/JamesHovious/w32"
)

// WindowsHotkeyManager controls and responds to hotkeys on the windows platform
type WindowsHotkeyManager struct {
	// clicker hotkeys interact with
	Clicker *Clicker
	// map of active hotkeys
	Hotkeys map[int]*WindowsHotkey
	// base ID to offset for hotkey registration
	baseID int
	// stop signal channel
	stop chan bool
	// channel for hotkey events
	events chan WindowsHotkeyEvent
	// channel to notify main loop of esired new registrations
	registration chan WindowsHotkey
	// channel to notify those who request registration of errors
	registrationError chan error
	// channel to notify main loop of desired deregistrations
	deregistration chan int
	// channel to notify those who request deregistration of errors
	deregistrationError chan error
}

// WindowsHotkeyCallback describes a function to be called when a hotkey binding is activated
type WindowsHotkeyCallback func(WindowsHotkeyEvent) error

// WindowsHotkey describes an individual hotkey binding for the windows platform
type WindowsHotkey struct {
	// ID the hotkey is registered with
	ID int
	// modifier keys that need to be pressed in order to trigger the hotkey
	Modifiers uint
	// virtual key that need to be pressed in order to trigger the hotkey
	Virtual uint
	// function called when hotkey is triggered
	Callback WindowsHotkeyCallback
}

// WindowsHotkeyEvent describes an individual instance of a hotkey being triggered
type WindowsHotkeyEvent struct {
	// the hotkey that was triggered
	Hotkey *WindowsHotkey
	// time since system start at the point the hotkey was triggered
	Timestamp time.Duration
	// position of the cursor on the screen at the time the hotkey was triggered
	CursorPosition w32.POINT
}

// NewWindowsHotkeyManager creates a new windows hotkey manager instance
func NewWindowsHotkeyManager(c *Clicker) *WindowsHotkeyManager {
	hm := new(WindowsHotkeyManager)
	hm.Clicker = c
	hm.baseID = rand.Intn(32768)
	hm.stop = make(chan bool)
	hm.events = make(chan WindowsHotkeyEvent, 16)
	hm.Hotkeys = make(map[int]*WindowsHotkey)
	return hm
}

func (hm *WindowsHotkeyManager) toggleCallback(event WindowsHotkeyEvent) error {
	hm.Clicker.Active = !hm.Clicker.Active
	if hm.Clicker.Active {
		hm.Clicker.Start()
	} else {
		hm.Clicker.Stop()
	}
	return nil
}

// getNewID returns an unused hotkey identifier to register with
func (hm *WindowsHotkeyManager) getNewID() int {
	return hm.baseID + len(hm.Hotkeys)
}

// Attach a callback to a hotkey
func (hm *WindowsHotkeyManager) Attach(modifiers, vkey uint, callback WindowsHotkeyCallback) error {
	id := hm.getNewID()
	for _, ok := hm.Hotkeys[id]; ok; {
		id++
	}
	log.Println("set hotkey", id)
	err := w32.RegisterHotKey(0, id, modifiers, vkey)
	if err == nil {
		hm.Hotkeys[id] = &WindowsHotkey{
			ID:        id,
			Modifiers: modifiers,
			Virtual:   vkey,
			Callback:  callback,
		}
	}
	return err
	/*
		hm.registration <- WindowsHotkey{
			ID:        id,
			Modifiers: modifiers,
			Virtual:   vkey,
			Callback:  callback,
		}
		return <-hm.registrationError */
}

// Detach removes a hotkey binding
func (hm *WindowsHotkeyManager) Detach(id int) error {
	hm.deregistration <- id
	return <-hm.deregistrationError
}

// DetachAll removes all hotkey bindings
func (hm *WindowsHotkeyManager) DetachAll() (err error) {
	for id := range hm.Hotkeys {
		err = hm.Detach(id)
		if err != nil {
			return err
		}
	}
	return nil
}

// loop executes the hotkey monitoring loop
func (hm *WindowsHotkeyManager) loop() {
	defer hm.DetachAll()
	var msg w32.MSG
	var r int
	for {
		select {
		case <-hm.stop:
			return
		case <-hm.registration:
			// placeholder
		default:
			r = w32.GetMessage(&msg, 0, w32.WM_HOTKEY, w32.WM_HOTKEY)
			log.Println("received hotkey", msg.WParam)
			hk, ok := hm.Hotkeys[int(msg.WParam)]
			if !ok {
				continue
			} else {
				log.Println("hotkey event r value", r)
			}
			event := WindowsHotkeyEvent{
				Hotkey:         hk,
				Timestamp:      time.Duration(time.Millisecond.Nanoseconds() * int64(msg.Time)),
				CursorPosition: msg.Pt,
			}
			hk.Callback(event)
		}
	}
}
