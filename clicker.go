package main

// ClickerConfig describes the configuration for the robot that automates clicks
type ClickerConfig struct {
	// click interval range
	Intervals IntervalRange
	// scaling multipliers
	Multipliers TickMultipliers
}

// ClickerStatistics describes statistics about the clicks made by the robot
type ClickerStatistics struct {
}

// Clicker describes the robot that automates clicks
type Clicker struct {
	// robot's configuration
	Config ClickerConfig

	// statistics about clicks
	Statistics ClickerStatistics

	// whether the clicker is currently clicking
	Active bool

	// ticker used to trigger click events
	ticker *ScaledRandomisedTicker
	// stop channel used to trigger click loop break
	stop chan bool
}

func NewDefaultClicker() *Clicker {
	ret := new(Clicker)
	ret.Config.Intervals.SetFrequencies(9, 15)
	ret.Config.Multipliers = *NewTickMultipliersFromInverses(0.25, 0.50, 0.75)
	ret.ticker = NewScaledRandomisedTicker(ret.Config.Intervals, ret.Config.Multipliers)
	ret.ticker.init()
	ret.stop = make(chan bool)
	return ret
}
