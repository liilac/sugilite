package main

import (
	"github.com/go-vgo/robotgo"
)

// Click executes a single mouse click
func (c *Clicker) Click() {
	robotgo.Click("left", false)
}

// loop continually waits on either a tick or a stop signal, clicking in case
// of tick, and stopping in case of stop signal
func (c *Clicker) loop() {
	for {
		select {
		case <-c.stop:
			return
		case <-c.ticker.C:
			c.Click()
		}
	}
}

// Start spawn a new click loop goroutine
func (c *Clicker) Start() {
	c.ticker.Start(false)
	go c.loop()
}

// Stop stops the click loop and corresponding ticker
func (c *Clicker) Stop() {
	c.stop <- true
	c.ticker.Stop()
}
