package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"math"
	"math/big"
	"time"
)

// IntervalRange describes a range of intervals; it has a minimum and maximum interval
type IntervalRange struct {
	Minimum time.Duration
	Maximum time.Duration
}

// SetFrequencies sets the intervals of the range to ones that correspond to the given frequencies
// note that minimumFrequency here corresponds to the Maximum field, and vice versa, due to the inverse relationship
// between frequency and interval
func (r *IntervalRange) SetFrequencies(minimumFrequency, maximumFrequency float64) {
	// calculate intervals in nanoseconds
	maximumIntervalFloat := 1e9 / minimumFrequency
	minimumIntervalFloat := 1e9 / maximumFrequency
	// set to derived truncated durations
	r.Maximum = time.Duration(int64(maximumIntervalFloat))
	r.Minimum = time.Duration(int64(minimumIntervalFloat))
}

// GetFrequencies gets the frequency equivalents of the intervals set
func (r IntervalRange) GetFrequencies() (minimumFrequency, maximumFrequency float64) {
	minimumFrequency = 1e9 / float64(r.Maximum.Nanoseconds())
	maximumFrequency = 1e9 / float64(r.Minimum.Nanoseconds())
	return
}

// randomInterval returns a randomised interval within the specified range
func (r IntervalRange) randomInterval() (time.Duration, error) {
	return r.scaledRandomInterval(1.0)
}

// randomIntervalMandatory returns a random interval within the given range if posible,
// and otherwise returns the maximum specified interval
func (r IntervalRange) randomIntervalMandatory() time.Duration {
	return r.scaledRandomIntervalMandatory(1.0)
}

// scaledRandomIntervalMandatory returns a random interval within the given range if possible,
// and otherwise returns the maximum specified interval, where the range is scaled by the given multiplier
func (r IntervalRange) scaledRandomIntervalMandatory(m float64) time.Duration {
	interval, err := r.scaledRandomInterval(m)
	if err != nil {
		log.Printf("using maximum value of %v: %v\n", r.Maximum, err)
		return r.Maximum
	}
	return interval
}

// scaledRandomInterval returns a randomised interval within the specified range, with the range scaled by the given multiplier
func (r IntervalRange) scaledRandomInterval(m float64) (time.Duration, error) {
	// calculate scaled minimum and maximum
	scaledMinimum := m * float64(r.Minimum)
	scaledMaximum := m * float64(r.Maximum)

	// calculate range between intervals
	intervalRange := math.Abs(scaledMaximum - scaledMinimum)

	// check intervalRange isn't zero
	// rand.Int() will panic if it is, so we just return the only possible value in this case
	if intervalRange == 0 {
		return time.Duration(int64(scaledMaximum)), nil
	}

	// get a random number in range [0, difference]
	intervalRangeBig, _ := big.NewFloat(intervalRange).Int(nil)
	differenceBig, err := rand.Int(rand.Reader, intervalRangeBig)
	if err != nil {
		// return 0, errors.Wrapf(err, "unable to generate random interval", 1)
		return 0, fmt.Errorf("unable to generate random interval: %v", err) // playground doesn't have smquartz/errors
	}
	difference := differenceBig.Int64()

	// derive the duration from the difference from minimum
	// calculate the lower of the maximum and minimum, in case they have been swapped
	lowerBound := int64(math.Min(scaledMaximum, scaledMinimum))
	// create a duration from the lower bound added to the random difference
	interval := time.Duration(lowerBound + difference)

	// return the interval
	return interval, nil
}
