// +build windows

package main

import (
	"log"
	"time"

	"github.com/JamesHovious/w32"

	_ "runtime/cgo"

	"github.com/lxn/walk"
)

func main() {

	clicker = NewDefaultClicker()

	hkm := NewWindowsHotkeyManager(clicker)
	err := hkm.Attach(0, w32.VK_F10, hkm.toggleCallback)
	log.Println("hot key attach error", err)

	app := walk.App()

	// These specify the app data sub directory for the settings file.
	app.SetOrganizationName("Lavender")
	app.SetProductName("Sugilite")

	// Settings file name.
	settings := walk.NewIniFileSettings("settings.ini")

	// All settings marked as expiring will expire after this duration w/o use.
	// This applies to all widgets settings.
	settings.SetExpireDuration(time.Hour * 24 * 30 * 3)

	if err := settings.Load(); err != nil {
		log.Fatal(err)
	}

	app.SetSettings(settings)

	if err := RunMainWindow(); err != nil {
		log.Fatal(err)
	}

	if err := settings.Save(); err != nil {
		log.Fatal(err)
	}
}
