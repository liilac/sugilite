package main

import (
	"github.com/lxn/walk/declarative"
)

func RunMainWindow() error {

	curMinFreq, curMaxFreq := clicker.Config.Intervals.GetFrequencies()

	if _, err := (declarative.MainWindow{
		Name:    "mainWindow", // Name is needed for settings persistence
		Title:   "Sugilite",
		MinSize: declarative.Size{Width: 500},
		MaxSize: declarative.Size{Width: 800},
		Layout:  declarative.VBox{},
		Children: []declarative.Widget{
			declarative.Label{
				Text: "Settings",
			},
			declarative.Composite{
				Layout: declarative.HBox{},
				Children: []declarative.Widget{
					declarative.Composite{
						Layout: declarative.HBox{},
						Children: []declarative.Widget{
							declarative.Label{
								Text: "Minimum clickrate",
							},
							declarative.NumberEdit{
								AssignTo:           &minCPSInput,
								Value:              curMinFreq,
								Decimals:           2,
								Suffix:             " CPS",
								ToolTipText:        "the minimum number of clicks that should be executed per second",
								OnValueChanged:     updateClickerFrequencies,
								AlwaysConsumeSpace: true,
							},
						},
					},
					declarative.Composite{
						Layout: declarative.HBox{},
						Children: []declarative.Widget{
							declarative.Label{
								Text: "Maximum clickrate",
							},
							declarative.NumberEdit{
								AssignTo:           &maxCPSInput,
								Value:              curMaxFreq,
								Decimals:           2,
								Suffix:             " CPS",
								ToolTipText:        "the minimum number of clicks that should be executed per second",
								OnValueChanged:     updateClickerFrequencies,
								AlwaysConsumeSpace: true,
							},
						},
					},
				},
			},
		},
		StatusBarItems: NewStatusBar(),
	}.Run()); err != nil {
		return err
	}

	return nil
}
