package main

import (
	"fmt"
	"time"

	"github.com/lxn/walk/declarative"
)

func generateClickerStateText(state bool) string {
	return fmt.Sprintf("state: %v", state)
}

func generateAverageCPSText(cps float64) string {
	return fmt.Sprintf("avg. CPS: %.2f CPS", cps)
}

func generateAverageClickLatencyText(latency time.Duration) string {
	return fmt.Sprintf("avg. latency: %v", latency.String())
}

func NewStatusBar() []declarative.StatusBarItem {
	return []declarative.StatusBarItem{
		declarative.StatusBarItem{
			Text:  "state: inactive",
			Width: 75,
		},
		declarative.StatusBarItem{
			Text:  "trigger: toggle,press,catch up",
			Width: 155,
		},
		declarative.StatusBarItem{
			Text:        generateAverageCPSText(12.34),
			ToolTipText: "the average number of clicks executed per second, while the clicker is active",
			Width:       110,
		},
		declarative.StatusBarItem{
			Text:        generateAverageClickLatencyText(time.Microsecond * 123),
			ToolTipText: "the average time between this program requesting a click, and the actual click",
			Width:       110,
		},
	}

}
