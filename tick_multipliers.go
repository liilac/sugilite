package main

// TickMultipliers describes a set of multipliers to apply to a interval range,
// where the index corresponds to the tick number, and ticks not covered use the multiplier 1
type TickMultipliers []float64

// Multiplier returns the multiplier appropriate for a given tick
func (tm TickMultipliers) Multiplier(tick int) float64 {
	if tick < len(tm) {
		return tm[tick]
	}
	return 1
}

// NewTickMultipliers returns a new TickMultipliers set from the given floats
func NewTickMultipliers(multipliers ...float64) *TickMultipliers {
	var tm = TickMultipliers(multipliers)
	return &tm
}

// NewTickMultipliersFromInverses returns a new TickMultipliers set, using the inverse of each value
// given for the corresponding multiplier
func NewTickMultipliersFromInverses(inverses ...float64) *TickMultipliers {
	var tm TickMultipliers
	for _, inverse := range inverses {
		tm = append(tm, 1/inverse)
	}
	return &tm
}
