package main

import (
	"time"
)

type KeybindEventType int

const (
	KeybindEventTypePress KeybindEventType = iota
	KeybindEventTypeRelease
	KeybindEventTypeHotkey
)

type KeybindEvent struct {
	Sequence  uint16
	Timestamp time.Time
	State     uint16
	Raw       interface{}
}

type KeybindCallback func(KeybindEvent, ...interface{})

type Keybinder interface {
	BindKey(key uint16, callback KeybindCallback) error
	UnbindKey(key uint16) error
}
