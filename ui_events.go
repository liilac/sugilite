package main

import (
	"log"

	"github.com/lxn/walk"
)

var minCPSInput *walk.NumberEdit
var maxCPSInput *walk.NumberEdit

var clicker *Clicker

func updateClickerFrequencies() {
	minFreq := minCPSInput.Value()
	maxFreq := minCPSInput.Value()
	clicker.Config.Intervals.SetFrequencies(minFreq, maxFreq)
	clicker.ticker.Intervals = clicker.Config.Intervals
	log.Println("updated clicker frequencies")
}
